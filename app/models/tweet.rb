# == Schema Information
#
# Table name: tweets
#
#  id         :integer          not null, primary key
#  body       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Foreign Keys
#
#  user_id  (user_id => users.id)
#
class Tweet < ApplicationRecord
  TWEET_TIME_THRESHOLD = 1.day.ago

  belongs_to :user

  validates :body, length: { maximum: 180 }
  before_commit :validate_tweet

  private

  def validate_tweet
    return false if tweet?

    true
  end

  def tweet?
    Tweet.where(user_id:, body:).where('created_at >= ?', TWEET_TIME_THRESHOLD).present?
  end
end
