module Api
  class TweetsController < ApplicationController
    skip_before_action :verify_authenticity_token

    TWEETS_PER_PAGE = 50

    def create
      return head :bad_request if params[:body].blank? || params[:user_id].blank?

      @tweet = Tweet.new(
        user_id: params[:user_id],
        body: params[:body]
      )

      if @tweet.save
        render json: @tweet, status: :ok
      else
        render json: {}, status: :unprocessable_entity
      end
    end

    def index
      debugger
      @tweets = Tweet.includes(user: :company).order(created_at: :asc).limit(TWEETS_PER_PAGE).offset(offset)

      render json: @tweets
    end

    private

    def page_params
      params.permit(:page).merge(per_page: TWEETS_PER_PAGE)
    end

    def page
      params[:page]
    end

    def affiliated?
      params[:affiliated]
    end

    def offset
      return 0 if !page || page == 1

      page * (page.to_i - 1)
    end
  end
end
