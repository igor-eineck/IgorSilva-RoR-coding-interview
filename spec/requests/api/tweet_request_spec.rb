require 'rails_helper'

RSpec.describe "API Tweets", type: :request do
  let(:response_body) { JSON.parse(response.body) }

  describe "#index" do
    context 'When there are tweets' do
      let!(:tweet_list) { create_list(:tweet, 2)}

      before do
        create_list(:tweet, 5)
      end

      it 'returns a successful response' do
        get api_tweets_path

        expect(response).to have_http_status(:success)
      end
    end
  end

  describe "#create" do
    context 'with valid parameters' do
      let(:user1) { create(:user)}
      let(:valid_body) { 'This is a valid tweet' }

      it 'returns a successful response' do
        post api_tweets_path(user_id: user1.id, body: valid_body)

        expect(response).to have_http_status(:success)
      end

      it 'creates a new tweet' do
        expect {
          post api_tweets_path(user_id: user1.id, body: valid_body)
        }.to change(Tweet, :count).by(1)
      end
    end

    context 'with valid parameters' do
      let(:user1) { create(:user)}
      let!(:tweet) { create(:tweet, user: user1, created_at: 12.hours.ago) }
      let(:valid_body) { 'This is a valid tweet' }

      it 'returns a successful response' do
        post api_tweets_path(user_id: user1.id, body: valid_body)

        expect(response).to have_http_status(:success)
      end

      it 'creates a new tweet' do
        expect {
          post api_tweets_path(user_id: user1.id, body: valid_body)
        }.to change(Tweet, :count).by(1)
      end
    end

    context 'With invalid parameters' do
      let(:user1) { create(:user)}

      context 'When the body is invalid' do
        let(:invalid_body) { '' }

        it 'returns an error response' do
          post api_tweets_path(user_id: user1.id, body: invalid_body)

          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'does not create a new tweet' do
          expect {
            post api_tweets_path(user_id: user1.id, body: invalid_body)
          }.to_not change(Tweet, :count)
        end
      end

      context 'When the body has more characters than allowed' do
        let(:invalid_body) do
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sit amet tellus massa. Vivamus gravida consequat elementum. Phasellus viverra mattis feugiat. Integer ultrices in ipsum vel hendrerit. Nunc egestas augue non magna tincidunt, sit amet egestas sem finibus. In faucibus, urna id porta imperdiet, lacus ex sollicitudin leo, ultrices faucibus eros ipsum sit amet elit. Duis fermentum nec lectus eu suscipit. Ut hendrerit ligula viverra consectetur ullamcorper. Nunc varius quam sit amet odio scelerisque pharetra non non sapien. Maecenas pretium dolor ante, vitae efficitur orci tristique in. Morbi at elit vitae ex condimentum luctus at nec arcu. Donec vitae ipsum interdum, cursus ligula quis, sollicitudin arcu. Morbi rutrum varius eleifend.
          Vivamus non rutrum dolor. Nam massa metus, viverra sit amet leo sit amet, blandit laoreet velit. Phasellus luctus tortor et tellus volutpat dignissim. Donec tortor nunc, pretium sit amet varius vel, mattis non diam. Morbi quis ultricies metus. Phasellus in ultricies velit. Cras sagittis nec ipsum quis placerat. Ut sit amet vulputate justo. Phasellus semper, orci in scelerisque imperdiet, diam risus rhoncus risus, in aliquet turpis neque quis ante. Sed aliquam dapibus orci sed ultrices. Morbi vel tincidunt purus, et pharetra mauris. Curabitur viverra odio vitae rhoncus euismod."
        end

        it 'returns an error response' do
          post api_tweets_path(user_id: user1.id, body: invalid_body)

          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'does not create a new tweet' do
          expect {
            post api_tweets_path(user_id: user1.id, body: invalid_body)
          }.to_not change(Tweet, :count)
        end
      end



      context 'When the tweet is invalid' do
        let(:body) { 'This is a valid tweet' }
        let!(:tweet1) { create(:tweet, user: user1, body: body) }

        it 'returns an error response' do
          post api_tweets_path(user_id: user1.id, body: body)

          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'does not create a new tweet' do
          expect {
            post api_tweets_path(user_id: user1.id, body: body)
          }.to_not change(Tweet, :count)
        end
      end
    end
  end
end
